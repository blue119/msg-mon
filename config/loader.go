// used to load toml configuration
package config

import (
	//  valid "github.com/asaskevich/govalidator"
	u "blue119/msg-mon/utils"
	"fmt"
	"github.com/BurntSushi/toml"
)

type tomlConfig struct {
	Gob_file string
	Telegram TelegramConf
	Services map[string]Service `toml:services`
}

type Service struct {
	Base_url string
	Depth    int
	Boards   []Board
}

type Board struct {
	Fullname string
	Name     string
	Url      string
	Match    string
	Skip     string
	Depth    int
	// scrape title
	// scrape content
	// grap all if it has no laster
}

type TelegramConf struct {
	Bot_api    string
	Chat_id    int64
	Parse_mode string
}

//  func (tc TelegramConf) init() bool {
//  }

// Load loads configuration
func Load(path string) error {
	var config tomlConfig
	//  var loader Loader
	//  loader = TOMLLoader{}
	//  return loader.Load(path, keyPass)
	if _, err := toml.DecodeFile(path, &config); err != nil {
		u.Log.Fatalln(err)
		return err
	}

	Conf.Gob_file = config.Gob_file

	Conf.Services = config.Services
	// update children field
	for serviceName, service := range Conf.Services {
		for i, board := range service.Boards {
			if board.Depth == 0 {
				service.Boards[i].Depth = service.Depth
			}

			if board.Fullname == "" {
				service.Boards[i].Fullname = fmt.Sprintf("%s:%s", serviceName, board.Name)
			}
		}
	}

	Conf.DoTeletram = false
	if config.Telegram.Bot_api != "" {
		Conf.DoTeletram = true
		Conf.Telegram = config.Telegram
	}

	return nil
}

// Loader is interface of concrete loader
//type Loader interface {
//	Load(string, string) error
//}
