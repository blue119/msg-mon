package config

import (
//  "errors"
//  "fmt"
//  "log/syslog"
//  "os"
//  "runtime"
//  "strconv"
//  "strings"

//  valid "github.com/asaskevich/govalidator"
//  "github.com/BurntSushi/toml"
//  log "github.com/sirupsen/logrus"
//  u "blue119/msg-mon/utils"
)

// Conf has Configuration
var Conf Config

type Config struct {
	Gob_file string

	// Input
	Services map[string]Service

	// Output
	DoTeletram bool
	Telegram   TelegramConf
}

//  func (c Config) GetConfig() (bconf Board) {
//  bconf := {}Board
//  return
//  }

//  func main() {
//  fmt.Println("vim-go")
//  }
