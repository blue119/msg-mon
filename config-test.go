package main

import (
	c "blue119/msg-mon/config"
	u "blue119/msg-mon/utils"
)

func main() {
	u.Log = u.NewCustomLogger()

	u.Log.Info("Load config.toml")
	c.Load("config.toml")

	u.Log.Info(c.Conf)
}
