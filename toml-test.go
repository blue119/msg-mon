package main

import (
	"fmt"
	//  "time"
	_ "regexp"

	"github.com/BurntSushi/toml"
)

var TITLE_EXAMPLE = [...]string{
	"[出售] 台南 Britax B-agile美規三輪推車",
	"[出售] 安全門欄 雙向開關 免打孔",
	"[出售] 全國 貝親第三階段15號鞋",
	"[出售] 全國 貝親奶瓶便宜出清",
	"[出售] 全國 全新 my awesome硬頁書",
	"[出售] 台北 Maxi-Cosi Cabriofix 提籃汽座",
	"[出售] 全國 3-12M包屁衣",
	"[出售] 全國 女童套裝100cm",
	"[出售] 全國 施巴嬰兒泡泡浴露禮盒",
	"[出售] 全國 兒童遊戲圍欄",
	"[出售] 全國 玩具出清",
	"[出售] 台中 nac nac fuzzy 智慧消毒烘乾鍋",
	"[出售] 全國 布書",
	"[出售] 全國 書-新生兒手冊、百歲、小雨麻副食品",
	"[出售] 全國 Love to dream  M號粉色一階",
	"[出售] 全國 mimos s",
	"[出售] 全國 OSHKOSH吊帶",
	"[出售] 售petitcollage 組裝立體拼圖，全國",
	"[出售] 高雄 幼兒腳踏車",
	"[出售] 全國 Quinny senzz 推車",
	"[出售] nacnac微電腦多功能溫奶器",
	"[出售] 北桃 quinny zapp boon flair 高腳餐椅",
	"[出售] 全國",
	"[出售] 台北 Maxi-cosi全新成長型汽座亞洲限定版",
	"[出售] 全國 Combi烘乾消毒鍋",
	"[出售] 全國 Snoopy 史努比 幼兒帽",
	"[出售] 全國 1-2歲女童上",
	"[出售] 台北 Cybex Priam 嬰兒推車黑架黑輪",
	"[出售] 新莊 嬰兒安全圍欄",
	"[出售] 全國 0-6M連身衣一批",
	"[出售] 雙北 2公分厚遊戲墊",
	"[出售] 桃園 南崁 costco Dwingular地墊",
}

type tomlConfig struct {
	//  Ptt    map[string]pttBoard
	//  Gtt    map[string]pttBoard
	//  Boards map[string][]boards
	Boards boards
	//  Boards struct {
	//  Ptt struct {
	//  BabyProduct struct {
	//  URL string `toml:"url"`
	//  Match string `toml:"match"`
	//  Skip string `toml:"skip"`
	//  } `toml:"BabyProduct"`
	//  NbShopping struct {
	//  URL string `toml:"url"`
	//  Match string `toml:"match"`
	//  Skip string `toml:"skip"`
	//  } `toml:"nb-shopping"`
	//  } `toml:"ptt"`
	//  } `toml:"boards"`

}

type boards struct {
	Gtt []pttBoard
	Ptt []pttBoard
}

type pttBoard struct {
	URL      string
	Skip     string
	MATCH    string
	BalaBala string
}

func main() {
	var config tomlConfig
	if _, err := toml.DecodeFile("config.toml", &config); err != nil {
		fmt.Println(err)
		return
	}

	//  fmt.Printf("%v\n", config)
	fmt.Printf("%v\n", config.Boards)
	//for boardName, board := range config.Ptt {
	//	match_re := regexp.MustCompile(board.MATCH)
	//	fmt.Printf("PTT: %s (%s, %s)\n", boardName, board.URL, board.MATCH)
	//	for _, v := range TITLE_EXAMPLE {
	//		if match_re.FindString(v) != "" {
	//			fmt.Printf("title: %s\n", v)
	//		}

	//	}
	//}
}
