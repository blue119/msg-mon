package main

import (
	u "blue119/msg-mon/utils"
)

func main() {
	u.Log = u.NewCustomLogger()
	u.Log.Debug("Debug")
	u.Log.Info("Info")
	u.Log.Warn("Warn")
	u.Log.Warning("Warning")
	u.Log.Error("Error")
	//  u.Log.Panic("panic")
	u.Log.Fatal("Fatal")
	u.Log.Print("Print")
}
