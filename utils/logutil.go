// refer to https://github.com/future-architect/vuls/tree/master/util
package utils

import (
	"os"
	//  "path/filepath"
	//  "runtime"

	//  "github.com/rifflock/lfshook"
	"github.com/sirupsen/logrus"
	prefixed "github.com/x-cray/logrus-prefixed-formatter"
)

// Log for localhsot
var Log *logrus.Entry

// NewCustomLogger creates logrus
//  func NewCustomLogger(c config.ServerInfo) *logrus.Entry {
func NewCustomLogger() *logrus.Entry {
	log := logrus.New()
	//  log.Formatter = &formatter.TextFormatter{MsgAnsiColor: c.LogMsgAnsiColor}
	log.Formatter = new(prefixed.TextFormatter)
	log.Out = os.Stderr
	//  log.Level = logrus.InfoLevel
	// TODO
	//  if config.Conf.Debug {
	log.Level = logrus.DebugLevel
	//  }

	// File output
	// TODO
	//logDir := GetDefaultLogDir()
	//if 0 < len(config.Conf.LogDir) {
	//	logDir = config.Conf.LogDir
	//}

	//if _, err := os.Stat(logDir); os.IsNotExist(err) {
	//	if err := os.Mkdir(logDir, 0700); err != nil {
	//		log.Errorf("Failed to create log directory: %s", err)
	//	}
	//}

	whereami := "localhost"
	//if 0 < len(c.ServerName) {
	//	whereami = c.GetServerName()
	//}

	//if _, err := os.Stat(logDir); err == nil {
	//	path := filepath.Join(logDir, whereami)
	//	log.Hooks.Add(lfshook.NewHook(lfshook.PathMap{
	//		logrus.DebugLevel: path,
	//		logrus.InfoLevel:  path,
	//		logrus.WarnLevel:  path,
	//		logrus.ErrorLevel: path,
	//		logrus.FatalLevel: path,
	//		logrus.PanicLevel: path,
	//	}, nil))
	//}

	fields := logrus.Fields{"prefix": whereami}
	return log.WithFields(fields)
}

// GetDefaultLogDir returns default log directory
//func GetDefaultLogDir() string {
//	defaultLogDir := "/var/log/vuls"
//	if runtime.GOOS == "windows" {
//		defaultLogDir = filepath.Join(os.Getenv("APPDATA"), "vuls")
//	}
//	return defaultLogDir
//}
