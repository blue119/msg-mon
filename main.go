package main

import (
	"encoding/gob"
	"fmt"
	"log"
	"net/http"
	"net/http/cookiejar"
	"net/url"
	"os"
	"regexp"
	"strings"

	c "blue119/msg-mon/config"
	u "blue119/msg-mon/utils"
	//  "github.com/BurntSushi/toml"
	"github.com/PuerkitoBio/goquery"
	"gopkg.in/telegram-bot-api.v4"
)

//type tomlConfig struct {
//	Scrape_max_depth int
//	Gob_file         string
//	Telegram         TelegramConf
//	Services         map[string]Service `toml:services`
//}
//type Service struct {
//	Base_url string
//	Boards   []Board
//}
//
//type Board struct {
//	Name  string
//	Url   string
//	Match string
//	Skip  string
//}

type TelegramConf struct {
	Bot_api    string
	Chat_id    int64
	Parse_mode string
}

type GobStruct map[string][]MSG

type MSG struct {
	Title string
	Url   string
}

const (
	scrape_max_depth = 2
)

func prettyMSG(msgs []MSG) {
	u.Log = u.NewCustomLogger()

	for _, v := range msgs {
		_len := 60
		if len(v.Title) <= _len {
			_len = len(v.Title)
		}
		//  u.Log.Debugf("%-60s %s\n", v.Title[:_len], v.Url)
		u.Log.Debugf("%s %s", v.Title[:_len], v.Url)
	}
}

// TODO: return error
func Scrape(client *http.Client, bconf c.Board, depth int, last_path string) []MSG {
	var ret_msg []MSG
	u.Log = u.NewCustomLogger()
	skip_re := regexp.MustCompile(bconf.Skip)
	get_url := bconf.Url

	u.Log.Infof("Path Depth %d", depth)

	// Request the HTML page.
	res, err := client.Get(get_url)
	if err != nil {
		log.Fatal(err)
	}
	defer res.Body.Close()
	if res.StatusCode != 200 {
		u.Log.Fatalf("status code error: %d %s", res.StatusCode, res.Status)
	}

	// Load the HTML document
	doc, err := goquery.NewDocumentFromReader(res.Body)
	if err != nil {
		u.Log.Fatal(err)
	}

	// over 18 year page
	if doc.Find(".over18-notice").Length() > 0 {
		//for _, nodes := range doc.Find("form").Nodes {
		//	for _, attr := range nodes.Attr {
		//		if attr.Key == "action" {
		//			fmt.Println(attr.Val)
		//		}
		//	}
		//}

		// fetch from url
		nxt_url := ""
		for _, nodes := range doc.Find("input").Nodes {
			for _, attr := range nodes.Attr {
				if attr.Key == "value" {
					nxt_url = attr.Val
				}
			}
		}
		res, _ := client.PostForm("https://www.ptt.cc/ask/over18",
			url.Values{"from": {nxt_url}, "yes": {"yes"}})

		doc, err = goquery.NewDocumentFromReader(res.Body)
		if err != nil {
			u.Log.Fatal(err)
		}
	}

	urlp, _ := url.Parse(get_url)
	base_url := fmt.Sprintf("%s://%s", urlp.Scheme, urlp.Host)

	//XXX ptt.cc is incremental list
	var _gss []goquery.Selection
	doc.Find(".r-ent").Each(func(i int, s *goquery.Selection) {
		// prepend
		_gss = append([]goquery.Selection{*s}, _gss...)
	})

	found_last_path := false
	for _, s := range _gss {
		title := s.Find(".title").Find("a").Text()
		title = strings.TrimSpace(title)
		// contain string show from config.toml
		if found_last_path || title == "" ||
			skip_re.FindString(title) != "" {
			continue
		}

		href, _ := s.Find(".title").Find("a").Attr("href")
		href = strings.TrimSpace(href)
		// XXX if the href match last_path just return
		if href == last_path {
			u.Log.Infof("Found the last path %s", last_path)
			found_last_path = true
			break
		}
		//  fmt.Println(href)

		date := s.Find(".meta .date").Text()
		date = strings.TrimSpace(date)

		full_url := fmt.Sprintf("%s%s", base_url, href)
		//  log.Printf("(%s) %s - %s\n", date, title, full_url)
		// prepend for ordering
		ret_msg = append(ret_msg, MSG{title, full_url})
	}

	u.Log.Debug("new msgs:")
	prettyMSG(ret_msg)

	//TODO: Stop if depth == 0 or found the last one
	if depth > 1 && !found_last_path {
		//doc.Find(".btn-group-paging a").Each(func(i int, s *goquery.Selection) {
		//	//  [0 最舊, 1 ‹ 上頁, 2 下頁, › 3 最新]
		//	fmt.Printf("%d %s\n", i, s.Text())
		//})

		last_page := doc.Find(".btn-group-paging a").Nodes[1].Attr[1].Val
		last_page_url := fmt.Sprintf("%s/%s", base_url, last_page)
		bconf.Url = last_page_url
		nxt_msg := Scrape(client, bconf, depth-1, last_path)
		ret_msg = append(ret_msg, nxt_msg...)
	}
	return ret_msg
}

func writeGob(filePath string, object interface{}) error {
	file, err := os.Create(filePath)
	if err == nil {
		encoder := gob.NewEncoder(file)
		err = encoder.Encode(object)
	}
	file.Close()
	return err
}

func readGob(filePath string, object interface{}) error {
	file, err := os.Open(filePath)
	if err == nil {
		decoder := gob.NewDecoder(file)
		err = decoder.Decode(object)
	}
	file.Close()
	return err
}

// TODO: error case
func InitTelegramAPI(c chan *tgbotapi.BotAPI, conf c.TelegramConf) {
	//  u.Log = u.NewCustomLogger()
	if bot, err := tgbotapi.NewBotAPI(conf.Bot_api); err != nil {
		u.Log.Panic(err)
		c <- nil
	} else {
		c <- bot
	}
	//  u.Log.Infof("Authorized on account %s", bot.Self.UserName)
}

// TODO: error case
func SentTelegramMSG(m chan tgbotapi.Message, msg string, bot *tgbotapi.BotAPI, conf c.TelegramConf) {
	u.Log = u.NewCustomLogger()
	_msg := tgbotapi.NewMessage(conf.Chat_id, msg)
	_msg.ParseMode = conf.Parse_mode
	if ret_msg, err := bot.Send(_msg); err != nil {
		u.Log.Errorf("%v", err)
		m <- tgbotapi.Message{}
	} else {
		m <- ret_msg
	}
}

func main() {
	///////////////////////////////////////////////////////////////////////////
	// Init Configuration
	u.Log = u.NewCustomLogger()
	c.Load("config.toml")

	// Read historical
	gobd := GobStruct{}
	if _, err := os.Stat(c.Conf.Gob_file); err == nil {
		if err := readGob(c.Conf.Gob_file, &gobd); err != nil {
			u.Log.Fatalln(err)
		}
	}
	///////////////////////////////////////////////////////////////////////////
	// TODO Init Output and it should run on defer check by chan after Input
	// inish

	new_msgs := map[string][]MSG{}

	// send mesagges to remote
	// TODO: DoTelegram
	out_c := make(chan *tgbotapi.BotAPI)
	go InitTelegramAPI(out_c, c.Conf.Telegram)

	for _, service := range c.Conf.Services {
		cookieJar, _ := cookiejar.New(nil)
		client := &http.Client{
			Jar: cookieJar,
		}

		for _, board := range service.Boards {
			u.Log.Infof("Fetch Service %s, url %s", board.Fullname, board.Url)
			last_path := ""
			last_title := ""
			//TODO: use fullname
			if _, ok := gobd[board.Name]; ok {
				u, _ := url.Parse(gobd[board.Name][0].Url)
				last_path = u.Path
				last_title = gobd[board.Name][0].Title
			}

			u.Log.Infof("%s (%s, %s)", board.Name, board.Url, board.Match)
			u.Log.Infof("last %s( %s%s )", last_title, service.Base_url, last_path)
			_msgs := Scrape(client, board, board.Depth, last_path)
			new_msgs[board.Fullname] = _msgs

			// update to db
			if _, ok := gobd[board.Name]; ok {
				gobd[board.Name] = append(_msgs, gobd[board.Name]...)
			} else {
				gobd[board.Name] = _msgs
			}

			err := writeGob(c.Conf.Gob_file, gobd)
			if err != nil {
				u.Log.Fatalln(err)
			}
			u.Log.Debug("========== Fetch Next ==========")
		}
	}

	bot_api := <-out_c
	_msg_c := 0
	//TODO: set capacity
	m := make(chan tgbotapi.Message)
	for _, service := range c.Conf.Services {
		for _, board := range service.Boards {
			u.Log.Debugf("========== Send %s ==========", board.Fullname)
			if msgs, ok := new_msgs[board.Fullname]; ok {
				for _, v := range msgs {
					match_re := regexp.MustCompile(board.Match)
					if match_re.FindString(v.Title) != "" {
						u.Log.Debugf("Title: %s %s", v.Title, v.Url)

						go SentTelegramMSG(m, v.Url, bot_api, c.Conf.Telegram)
						_msg_c += 1
					}
				}
			}
		}
	}
	for i := 0; i < _msg_c; i++ {
		<-m
	}
	u.Log.Debugf("========== All(%d) Send Done ==========", _msg_c)
}
